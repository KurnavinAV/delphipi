{**
 DelphiPI (Delphi Package Installer)
 Author      : ibrahim dursun (ibrahimdursun gmail)
 Contributor : ronald siekman
 License     : GNU General Public License 2.0
**}
unit PageCompilerOptions;

interface

uses
  Classes, CompilationData, Windows, Messages, SysUtils, Variants, Graphics, Vcl.Controls, Forms,
  Dialogs, PageBase, StdCtrls, ExtCtrls, WizardIntfs, Vcl.Buttons,
  System.ImageList, Vcl.ImgList, System.Actions, Vcl.ActnList;

type
  TSelectCompilerOptions = class(TWizardPage)
    grpOutputFolders: TGroupBox;
    btnBPLBrowse: TButton;
    btnDCPBrowse: TButton;
    btnDCUBrowse: TButton;
    ActionList: TActionList;
    actSelectBPL: TAction;
    ImageList: TImageList;
    panelBPL: TPanel;
    buttonBPL: TSpeedButton;
    lblBPLOutputFolder: TLabel;
    edtBPL: TEdit;
    panelDirectives: TPanel;
    panelDCU: TPanel;
    buttonDCU: TSpeedButton;
    panelDCP: TPanel;
    buttonDCP: TSpeedButton;
    lblDCP: TLabel;
    edtDCP: TEdit;
    lblDCU: TLabel;
    edtDCU: TEdit;
    labelDirectives: TLabel;
    edtConditionals: TEdit;
    actSelectDCP: TAction;
    actSelectDCU: TAction;
    chkBuildWin64: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure buttonBPLClick(Sender: TObject);
    procedure buttonDCPClick(Sender: TObject);
    procedure buttonDCUClick(Sender: TObject);
    procedure actSelectBPLExecute(Sender: TObject);
    procedure actSelectDCPExecute(Sender: TObject);
    procedure actSelectDCUExecute(Sender: TObject);
  private
    procedure SaveInstallationOutputFolders;
    procedure ShowInstallationOutputFolders;
  public
    constructor Create(Owner: TComponent; const compilationData: TCompilationData); override;
    procedure UpdateWizardState; override;
    function CanShowPage: Boolean; override;
  end;

var
  SelectCompilerOptions: TSelectCompilerOptions;

implementation

uses gnugettext, Utils, JclIDEUtils;

{$R *.dfm}

{ TSelectDelphiInstallationPage }

constructor TSelectCompilerOptions.Create(Owner: TComponent;
  const compilationData: TCompilationData);
begin
  inherited;
  TranslateComponent(self);
  FCompilationData := compilationData;
end;

procedure TSelectCompilerOptions.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  SaveInstallationOutputFolders;
end;

procedure TSelectCompilerOptions.FormCreate(Sender: TObject);
begin
  inherited;
  TranslateComponent(self);
  ShowInstallationOutputFolders;
end;

procedure TSelectCompilerOptions.actSelectBPLExecute(Sender: TObject);
begin
  SelectFolder(edtBPL);
end;

procedure TSelectCompilerOptions.actSelectDCPExecute(Sender: TObject);
begin
  SelectFolder(edtDCP);
end;

procedure TSelectCompilerOptions.actSelectDCUExecute(Sender: TObject);
begin
  SelectFolder(edtDCU);
end;

procedure TSelectCompilerOptions.buttonBPLClick(Sender: TObject);
begin
  SelectFolder(edtBPL);
end;

function TSelectCompilerOptions.CanShowPage: Boolean;
begin
   Result := True;
  //Result := FCompilationData.Scripting;
end;

procedure TSelectCompilerOptions.SaveInstallationOutputFolders;
begin
  Assert(Assigned(fCompilationData), 'Compilation data is null');
  fCompilationData.BPLOutputFolder := edtBPL.Text;
  fCompilationData.DCPOutputFolder := edtDCP.Text;
  fCompilationData.DCUOutputFolder :=
    RelativeToAbsolutePath(ExtractFilePath(ParamStr(0)), edtDCU.Text);
  fCompilationData.Conditionals := edtConditionals.Text;
  fCompilationData.BuildWin64 := chkBuildWin64.Checked;
end;

procedure TSelectCompilerOptions.ShowInstallationOutputFolders;
begin
  Assert(Assigned(fCompilationData), 'Compilation data is null');
  edtBPL.Text:= fCompilationData.BPLOutputFolder;
  edtDCP.Text:= fCompilationData.DCPOutputFolder;
  edtDCU.Text:= fCompilationData.DCUOutputFolder;
  if (edtDCU.Text = '') then
    edtDCU.Text := RelativeToAbsolutePath(ExtractFilePath(ParamStr(0)), 'win32');

  edtConditionals.Text := fCompilationData.Conditionals;
  chkBuildWin64.Checked := fCompilationData.BuildWin64;
  chkBuildWin64.Visible := (clDcc64 in fCompilationData.Installation.CommandLineTools);
  if not(chkBuildWin64.Visible) then
  begin
    grpOutputFolders.Height := chkBuildWin64.Top;
  end;
end;

procedure TSelectCompilerOptions.buttonDCUClick(Sender: TObject);
begin
  SelectFolder(edtDCU);
end;

procedure TSelectCompilerOptions.buttonDCPClick(Sender: TObject);
begin
  SelectFolder(edtDCP);
end;

procedure TSelectCompilerOptions.UpdateWizardState;
begin
  inherited;
  wizard.SetHeader(_('Select Output Folders and Compiler Conditionals'));
  wizard.SetDescription(_('Please select output folders and compiler conditionals that will affect the compilation'));
end;

end.
