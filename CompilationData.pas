{**
 DelphiPI (Delphi Package Installer)
 Author      : ibrahim dursun (ibrahimdursun gmail)
 Contributor : ronald siekman
 License     : GNU General Public License 2.0
**}
unit CompilationData;
interface
uses Classes, PackageInfo, PackageList, JclIDEUtils, Generics.Collections;

type
  TFolder = class
  private
    FName: string;
    FTemporary: Boolean;
  public
    constructor Create(AName: string; ATemporary: Boolean = False);
    property Name: string read FName write FName;
    property Temporary: Boolean read FTemporary write FTemporary;
  end;

  TCompilationData = class
  private
    fBaseFolder: String;
    fInstallation: TJclBorRADToolInstallation;
    fPackageList: TPackageList;
    fHelpFiles: TStringList;
    fResourceFiles: TStringList;
    fPrecompileCommands: TStringList;
    fPattern: String;
    fDCPOutputFolder: string;
    fBPLOutputFolder: string;
    fDCUOutputFolder: string;
    fConditionals: string;
    fSearchPaths: TList<TFolder>;
    fBrowsingPaths: TList<TFolder>;
    fScripting: Boolean;
    fCleanup: Boolean;
    fBuildWin64: Boolean;
    fNameSpace: string;
    fDebugDCUPaths: TList<TFolder>;
    fIsDebug: Boolean;

    procedure SetPackageList(const aPackageList: TPackageList);
    procedure SetInstallation(const Value: TJclBorRADToolInstallation);
  protected
  public
    constructor Create;
    destructor Destroy; override;

    procedure GetIdePackages(const list: TStringList); virtual;
    function GetIdeVersionSuffix: string; virtual;
    function SetDelphiVersion(const version:string):boolean; virtual;

    property Pattern: String read fPattern write fPattern;
    property Installation: TJclBorRADToolInstallation read fInstallation write SetInstallation;
    property BaseFolder: String read fBaseFolder write fBaseFolder;
    property HelpFiles: TStringList read fHelpFiles;
    property ResourceFiles: TStringList read fResourceFiles;
    property PrecompileCommands: TStringList read fPrecompileCommands;
    property PackageList: TPackageList read fPackageList write SetPackageList;
    property DCPOutputFolder: string read fDCPOutputFolder write fDCPOutputFolder;
    property BPLOutputFolder: string read fBPLOutputFolder write fBPLOutputFolder;
    property DCUOutputFolder: string read fDCUOutputFolder write fDCUOutputFolder;
    property Conditionals: string read fConditionals write fConditionals;

    property SearchPaths: TList<TFolder> read fSearchPaths;
    property BrowsingPaths: TList<TFolder> read fBrowsingPaths;
    property DebugDCUPaths: TList<TFolder> read fDebugDCUPaths;

    property BuildWin64: Boolean read fBuildWin64 write fBuildWin64;
    property NameSpace: string read fNameSpace write fNameSpace;

    property Scripting: Boolean read fScripting write fScripting;
    property CleanupDCUOutputFolder: Boolean read fCleanup write fCleanup;
    property IsDebug: Boolean read fIsDebug write fIsDebug;
  end;

var
  ACTIVE_PLATFORM: TJclBDSPlatform = bpWin32;

implementation

uses JclFileUtils,SysUtils;
var
  installations: TJclBorRADToolInstallations;

constructor TCompilationData.Create;
begin
  fPattern := '*.dpk';
  fPackageList := TPackageList.Create;
  fHelpFiles := TStringList.Create;
  fResourceFiles := TStringList.Create;
  fPrecompileCommands := TStringList.Create;
  fSearchPaths := TList<TFolder>.Create;
  fBrowsingPaths := TList<TFolder>.Create;
  fDebugDCUPaths := TList<TFolder>.Create;
  fBuildWin64 := True;
  fNameSpace := 'System;System.Win;WinAPI;Vcl;Vcl.Imaging;Vcl.Samples;Data;VCLTee;XML;DataSnap;Data.Win';

  fScripting := False;
  fCleanup := False; { DelphiPIConsole }
  fIsDebug := False;
end;

destructor TCompilationData.Destroy;
var
  folder: TFolder;

begin
  fPackageList.Free;
  fHelpFiles.Free;
  fResourceFiles.Free;
  fPrecompileCommands.Free;

  for folder in fSearchPaths do begin
    if (folder.Temporary) then begin
      Installation.RemoveFromLibrarySearchPath(folder.Name, bpWin32);
      if (clDcc64 in Installation.CommandLineTools) then
        Installation.RemoveFromLibrarySearchPath(folder.Name, bpWin64);
    end;

    folder.Free;
  end;
  fSearchPaths.Free;

  for folder in fBrowsingPaths do
    folder.Free;
  fBrowsingPaths.Free;

  for folder in fDebugDCUPaths do
    folder.Free;
  fDebugDCUPaths.Free;

  inherited;
end;

procedure TCompilationData.GetIdePackages(const list: TStringList);
var
 i: integer;
begin
  assert(Assigned(Installation),'installation cannot be null');
 
  for i := 0 to Installation.IdePackages.Count - 1 do
    list.Add(Installation.IdePackages.PackageFileNames[i]);
end;

function TCompilationData.GetIdeVersionSuffix: string;
begin
  Result := Installation.VersionNumberStr;
  if Result = 'd11' then //Delphi 2007 packages have version 10 extension
    Result := 'd10';
end;

function TCompilationData.SetDelphiVersion(const version: string): boolean;
var
  installation: TJclBorRADToolInstallation;
  i : integer;
begin
  Result := false;
  for i := 0 to installations.Count - 1 do
  begin
    installation := installations.Installations[i];
    if UpperCase(Trim(installation.VersionNumberStr)) = UpperCase(Trim(version)) then
    begin
      fInstallation := installation;
      Result := true;
      break;
    end;
  end;
  if fInstallation = nil then
    raise Exception.Create('cannot find delphi version:' + version);
end;

procedure TCompilationData.SetInstallation(
  const Value: TJclBorRADToolInstallation);
begin
  if fInstallation = Value then
    exit;
    
  fInstallation := Value;

  BPLOutputFolder := fInstallation.BPLOutputPath[ACTIVE_PLATFORM];
  DCPOutputFolder := fInstallation.DCPOutputPath[ACTIVE_PLATFORM];
end;

procedure TCompilationData.SetPackageList(const aPackageList: TPackageList);
begin
  fPackageList.Free;
  fPackageList := aPackageList;
end;
{ TFolder }

constructor TFolder.Create(AName: string; ATemporary: Boolean);
begin
  FName := AName;
  FTemporary := ATemporary;
end;

initialization
  installations := TJclBorRADToolInstallations.Create;
finalization
  installations.Free;  

end.
